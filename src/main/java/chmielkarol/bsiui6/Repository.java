package chmielkarol.bsiui6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

@org.springframework.stereotype.Repository
public class Repository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public String queryWithSqlInjection(String name) {
        return jdbcTemplate.queryForList("SELECT * FROM PEOPLE WHERE NAME = '" + name + "' AND PUBLIC = TRUE").toString();
    }

    public String querySafe(String name){
        String getUserWithName = "SELECT * FROM PEOPLE WHERE NAME = ? AND PUBLIC = TRUE";
        return jdbcTemplate.queryForList(getUserWithName, name).toString();
    }
}