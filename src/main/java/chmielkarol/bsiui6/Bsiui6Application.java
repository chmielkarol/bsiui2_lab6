package chmielkarol.bsiui6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@SpringBootApplication
public class Bsiui6Application implements CommandLineRunner {

	@Autowired
	Repository repository;

	public static void main(String[] args) {
		SpringApplication.run(Bsiui6Application.class, args);
	}

	@Override
	public void run(String...args) throws Exception {

		BufferedReader reader =
				new BufferedReader(new InputStreamReader(System.in));
		while(true){

			System.out.println("SQL Injection | Podaj nazwisko: ");
			String name = reader.readLine();
			System.out.println(repository.queryWithSqlInjection(name));

			System.out.println("Safe | Podaj nazwisko: ");
			name = reader.readLine();
			System.out.println(repository.querySafe(name));
		}
	}

}
