DROP TABLE IF EXISTS people;

CREATE TABLE people (
                        id INT AUTO_INCREMENT  PRIMARY KEY,
                        name VARCHAR(250) NOT NULL,
                        public BOOLEAN NOT NULL

);